`use strict`;


function createNewUser() {
    let user = {};

    Object.defineProperty(user, 'firstName', {
        value: prompt('Enter name'),
        writable: false
    });
    Object.defineProperty(user, 'lastName', {
        value: prompt('Enter surname'),
        writable: false
    });

    setFirstName = function (value) {
        Object.defineProperty(user, 'firstName', {
            writable: true
        });
        user.firstName = value;
    };
    setLastName = function (value) {
        Object.defineProperty(user, 'lastName', {
            writable: true
        });
        user.lastName = value;
    };

    user.getLogin = function () {
        return (user.firstName.charAt(0).toLowerCase() + user.lastName.toLowerCase());
    };

    return user;
}

const user = createNewUser();


user.getLogin();
console.log(user.getLogin());
